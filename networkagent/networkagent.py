from epynet import Network
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 


class NetworkAgent:

    def __init__(self, filename='anytown_master.inp'): 
        self.network = Network(filename)
        self.network.solve()
        self.speed_limit = (0.7, 1.2)
        # danger zone 
        self.junction_threshold = (15, 120)
        self.state = None
        self.eff = None
        self.stepsize = 0.1


    def _calc_effeciency(self, curve_id):
        curve = None
        for x in self.network.curves:
            if curve_id in x.uid:
                curve = np.poly1d(np.polyfit([x[0] for x in x.values],[x[1] for x in  x.values],4))

        self.eff = curve

    def _calc_reward(self):
        print(self.network.junctions.values())
        print(self.network.pumps.speed)


    def reset(self):
        self._calc_effeciency('E1')
        self.network.solve()
        self._calc_reward()


    def step(self):
        self.network.solve()

    
    def calc_eff(self):
        #pumps  = self.network.pumps#.properties
        #### this is for the new more complex network  ####
        # for now the two pumps are interconnected, so we only need ont 
        #for pumpid,pump in pumps.items():
        #    pump.properties['flow']
        #true_flow = self.network.pumps['flow']#.properties

        # get only one pump with id 
        flow  = self.network.pumps['78'].flow
        print('flow is: {}'.format(flow))

        speed = self.network.pumps['78'].speed

        true_flow = float(flow/speed)
        print('True flow is : {}'.format(true_flow))
        return self.eff(true_flow)
        #self.eff(


    def junction_head(self):
        return list(self.network.junctions.head)

    def junction_demand(self):
        return list(self.network.junctions.demand)

            
    def info(self):
        print('speed')
        print(self.network.pumps.speed)
        print('junctions')
        print(self.network.junctions.basedemand)
        print(self.network.junctions.pressure)
        print('pump')
        print(self.network.pumps)


    def change_speed(self, speed):
        self.network.pumps.speed = speed
    
    def _action_increase_speed(self):
        # check if it's within the limits
        if self.network.pumps['78'].speed + self.stepsize > self.speed_limit[1]:
            return
        self.network.pumps['78'].speed += self.stepsize


    def _action_decrease_speed(self):
        if self.network.pumps['78'].speed - self.stepsize < self.speed_limit[1]:
            return
        self.network.pumps['78'].speed -= self.stepsize



if __name__ == '__main__':
    na  = NetworkAgent('anytown_master.inp')
    na.reset()
    junctions = [] 
    demands = [] 

    for i in range(10):
        """ step and increase speed """
        na._action_increase_speed()
        print('junction head')
        junction = na.junction_head()
        print('junction demand')
        jd = na.junction_demand()
        junctions.append(junction)
        demands.append(jd)
        na.step()

    for i in range(10):
        """ step and increase speed """
        na._action_decrease_speed()
        junction = na.junction_head()
        jd = na.junction_demand()
        junctions.append(junction)
        demands.append(jd)
        na.step()
    print(junctions[0]) 
    print(demands[0])
    print(junctions[1])
